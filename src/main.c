#include "wasm4.h"
#include "graphics.h"
#include "gamepad-utils.h"
#include "map.h"

const unsigned char FALSE = 0;
const unsigned char TRUE = 1;
const char RIGHT = 0;
const char LEFT = 1;

int cameraX = 0;
int cameraY = 0;;
int catX = 80;
int catY = 80;
int catVelX = 0;
int catVelY = 0;
unsigned char catState = 1;
unsigned char catAnimFrame = 0;
unsigned char catAnimDelay = 0;
char catFacing = RIGHT; 

//Cat States
const unsigned int CAT_IDLE = 1;
const unsigned int CAT_WALK = 2;
  //TODO Add more cat states

//Draw Background
//void drawBackground(){
//TODO
//}
    
//Draw Tiles
void drawTiles(){
  
  //Adjust draw palette
  *DRAW_COLORS = 0x4320;
  
  const int tSize = (int)tileSize;
  //Determine Offset
  int offX = cameraX % tSize;
  int offY = cameraY % tSize; 

  const int screenW = 160 / tSize;
  const int screenH = 160 / tSize;

  //Draw the tiles
  for(int h = 0; h < screenH + 1; h++){
    for(int w = 0; w < screenW + 1; w++){
      int drawX = cameraX + w * tSize;
      int drawY = cameraY + h * tSize;
      unsigned char t = getTileAt(drawX, drawY);
      drawX = w * tSize - offX;
      drawY = h * tSize - offY;

      spr(t, drawX, drawY, RIGHT);
    }
  }
}

unsigned char whichSprite(unsigned char state){
  //Determine cat sprite based on state and
  //  animation frame
  
  unsigned char sprite = 0;
  const unsigned char CAT_WALK_DELAY = 10; //delay in frames

  if(catState == CAT_IDLE){
    sprite = state;
    catAnimFrame = 0;
    catAnimDelay = 0;
  }
  if(catState == CAT_WALK){
    sprite = catState + catAnimFrame;
    if(catAnimDelay == 0){
      catAnimDelay = CAT_WALK_DELAY; 
      catAnimFrame += 1;
    }
    catAnimDelay -= 1;
    if(catAnimFrame > 1) catAnimFrame = 0;
  }

  return sprite;

}
    
//Draw Cat
void drawCat(){

  //Adjust draw palette
  *DRAW_COLORS = 0x4320;

  //Find cat position on screen 
  //based on actual X Y and camera
  int drawX = catX - cameraX - ((int)tileSize / 2);
  int drawY = catY - cameraY - (int)tileSize + 1;
  
  //Determine cat sprite based on state and
  //  animation frame
  unsigned char catSprite = whichSprite(catState);

  //Draw cat sprite based on
    //cat position
    //cat state
    //cat facing 
  spr(catSprite,drawX,drawY,catFacing);

  //TODO Add walking animation
  
}

unsigned char onGround(){

  const int tSize = (int)tileSize;
  unsigned char on_ground = FALSE; 

  //check velocity is zero
  if(catVelY != 0){return FALSE;}

  //check solid block underneath
  int checkY = catY + 1;
  if(solidTile(catX,checkY)){return TRUE;}

  return on_ground;
}

int catMoveBuffer(){
  int buffer = (int)tileSize / 2;
  if(catVelX > 0){ return buffer;}
  if(catVelX < 0){ return buffer * -1;}
  return 0;
}
//Move Cat
void moveCat(){

  //TODO Adjust gravity and jump speed
  const int CAT_JUMP_SPEED = -10;
  const int GRAVITY = 1;

  //Get buttons from gamepad
  unsigned char gamepad = *GAMEPAD1;

  //Apply Horizontal movement
  catVelX = padX(gamepad);

  //Apply Jump movement
  unsigned char jump = gamepad & BUTTON_1;
  if(jump && onGround()){
    catVelY = CAT_JUMP_SPEED;
  }

  //Apply Gravity
  catVelY = catVelY + GRAVITY;

  //Check vertical collision and move
  int newCatY = catY + catVelY;
  if(!(solidTile(catX, newCatY))){catY = newCatY;}
  else{catVelY = 0;}

  //TODO Check ceiling collision

  //Check horizontal collision and move
  int newCatX = catX + catVelX + catMoveBuffer();
  if(!(solidTile(newCatX, catY))){catX = newCatX - catMoveBuffer();}
  else{catVelX = 0;}

  //Determine cat state
  if(catVelX == 0 && catVelY == 0){catState = CAT_IDLE;}
  if(catVelX != 0 && catVelY == 0){catState = CAT_WALK;}
  //TODO Add jump state

  //Determine cat facing
  if(catVelX > 0){catFacing = RIGHT;}
  else if(catVelX < 0){catFacing = LEFT;}

}	

//Update Map

int prevCatY = 0;
int prevCatX = 0;

void debug_game(){
  //debug stuff here
  if(prevCatY != catY){
    tracef("Cat Y = %d", catY);
    prevCatY = catY;
  }
  if(prevCatX != catX){
    tracef("Cat X = %d", catX);
    prevCatX = catX;
  }
}


void update () {

  //Change Palette

  //Draw Background
  //drawBackground();
    
  //Draw Tiles
  drawTiles();
    
  //Draw Cat
  drawCat();

  //Move Cat
  moveCat();
    
  //Update Map
    
  //Play sounds

  debug_game();
}

