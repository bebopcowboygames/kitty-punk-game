int padX(unsigned char gamepad){
  int xValue = 0;
  if(gamepad & BUTTON_LEFT){ xValue = xValue - 1; }
  if(gamepad & BUTTON_RIGHT){ xValue = xValue + 1; }
  return xValue;
}

int padY(unsigned char gamepad){
  int yValue = 0;
  if(gamepad & BUTTON_UP){ yValue = yValue - 1; }
  if(gamepad & BUTTON_DOWN){ yValue = yValue + 1; }
  return yValue;
}
