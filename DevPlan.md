# KittyPunk

A game about a cat in a cyberpunk world with some obvious inspiration from recent game releases.

Cat
- Location x,y
- Velocity x,y
- State
  - Idle
  - Walking L,R
  - Jumping L,R


Map
- Tiles
- Camera x,y,w,h
  - Screen Tiles

Background

Palette Color Change
Rain Effect

Update
- Draw Cat
- Draw Map
- Move Cat
- Change Map

Probably should come up with some kind of story
